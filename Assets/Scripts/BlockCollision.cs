﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCollision : MonoBehaviour
{

	void Start()
	{
		Vector3   position = transform.position; position.y += 0.05f;
		transform.position = position;
	}

	void OnCollisionEnter(Collision other)
	{
		if (
		    (int)(transform.position.y*-100) !=
		    (int)(other.gameObject.transform.position.y*-100)) return;

		Vector3   position = transform.position; position.y += 0.1f;
		transform.position = position;
	}
}
