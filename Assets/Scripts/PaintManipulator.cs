using UnityEngine;
using UnityEngine.EventSystems;
using GoogleARCore;

#if UNITY_EDITOR
using Input = GoogleARCore.InstantPreviewInput;
#endif


public class PaintManipulator : MonoBehaviour
{
	public GameObject    DrawPrefab;

	private GameObject    obj;
	private LineRenderer line;
	private Vector3      prev;

	void Update()
	{

		Touch touch;
		if (Input.touchCount < 1)
		{
			return;
		}

		touch = Input.GetTouch(0);

		if (touch.phase == TouchPhase.Moved)
		{

			Vector3 spawn = Camera.main.ScreenToWorldPoint(
						new Vector3(touch.position.x,
						    touch.position.y, 45));

			Instantiate(DrawPrefab, spawn, Camera.main.transform.rotation);
		}

	}
}
