using UnityEngine;
using UnityEngine.EventSystems;
using GoogleARCore;
using GoogleARCore.Examples.ObjectManipulation;


public class BuilderManipulator : Manipulator
{
	public Camera FirstPersonCamera;

	public GameObject PawnPrefab;

	public GameObject ManipulatorPrefab;

	protected override bool CanStartManipulationForGesture(TapGesture gesture)
	{

		if(EventSystem.current.IsPointerOverGameObject(gesture.FingerId))
		{
			Debug.Log("Touched the UI");
			return false;
		}

		return true;
	}

	protected override void OnEndManipulation(TapGesture gesture)
	{
		if (gesture.WasCancelled)
		{
			return;
		}

		// Raycast against the location the player touched to search for planes.
		TrackableHit hit;
		TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon;

		if (Frame.Raycast(
					gesture.StartPosition.x, gesture.StartPosition.y, raycastFilter, out hit))
		{

			Vector3 position = hit.Pose.position;

			// Instantiate game object at the hit pose.
			var gameObject = Instantiate(PawnPrefab, position, hit.Pose.rotation);

			// Compensate for the hitPose rotation facing away from the raycast (i.e.
			// camera).
			gameObject.transform.Rotate(0, 180.0f, 0, Space.Self);

			// Instantiate manipulator.
			var manipulator =
				Instantiate(ManipulatorPrefab, position, hit.Pose.rotation);

			// Make game object a child of the manipulator.
			gameObject.transform.parent = manipulator.transform;

			// Create an anchor to allow ARCore to track the hitpoint as understanding of
			// the physical world evolves.
			var anchor = hit.Trackable.CreateAnchor(hit.Pose);

			// Make manipulator a child of the anchor.
			manipulator.transform.parent = anchor.transform;

			// Select the placed object.
			manipulator.GetComponent<Manipulator>().Select();
		}
	}
}
