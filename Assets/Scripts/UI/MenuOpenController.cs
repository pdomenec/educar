﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.Examples.ObjectManipulation;


public class MenuOpenController : MonoBehaviour
{

	public GameObject obj;

	public void OnClickActive()
	{
		obj.SetActive(true);
	}

	public void OnClickNoActive()
	{
		obj.SetActive(false);
	}

	public void OnClickDeleteModel()
	{
		if (ManipulationSystem.Instance.SelectedObject != null)
		{
			Debug.Log("Remove Model " + ManipulationSystem.Instance.SelectedObject);
			Destroy(ManipulationSystem.Instance.SelectedObject);
		}
	}
}
