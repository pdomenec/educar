﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore;

using GoogleARCore.Examples.ObjectManipulation;

public class SelectorPrefab : MonoBehaviour
{
	public GameObject SkeletonController;
	public GameObject GameObjectPrefab;

	public void OnClick()
	{
		// Muy mejorable, ...
		if (SkeletonController.GetComponent<PawnManipulator>())
			SkeletonController.GetComponent<PawnManipulator>().PawnPrefab    = GameObjectPrefab;
		if (SkeletonController.GetComponent<BuilderManipulator>())
			SkeletonController.GetComponent<BuilderManipulator>().PawnPrefab = GameObjectPrefab;
	}
}
