﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Controller : MonoBehaviour
{

	public Texture2D screenshot;
	public bool      fadeEnabled;
	public bool      backButton   = false;
	public string    lastScene;
	public float     speed        = 50;


	private CanvasGroup canvasGroup;
	private Image       image;
	private GameObject  fade;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{

		Debug.Log("OnSceneLoaded: " + scene.name);
		Debug.Log(mode);

		fade = GameObject.Find("Fade");

		if (fade != null)
		{

			if (!fadeEnabled)
			{
				fade.SetActive(false);
			}
		}
	}

	void OnDisable()
	{
		Debug.Log("OnDisable");
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	public void ChangeScene(string scene)
	{

		Screen.orientation = ScreenOrientation.AutoRotation;
		Scene _scene = SceneManager.GetActiveScene();
		lastScene = _scene.name;

		SceneManager.LoadScene(scene);
	}

	IEnumerator LoadScene(string scene)
	{
		yield return new WaitForEndOfFrame();

		RenderTexture.active = null;

		screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, false);

		screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		screenshot.Apply();

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);

		asyncLoad.allowSceneActivation = false;

		while (!asyncLoad.isDone)
		{
			if (asyncLoad.progress >= 0.9f)
			{
				asyncLoad.allowSceneActivation = true;
			}
			yield return null;
		}

	}

	public void OnClickWithFade(string scene)
	{
		fadeEnabled = true;
		StartCoroutine(LoadScene(scene));
	}

	public void OnClick(string scene)
	{
		fadeEnabled = false;
		ChangeScene(scene);
	}

	void Update()
	{
		if (fadeEnabled)
		{

			GameObject canvas = GameObject.Find("Canvas");

			if (canvas != null)
			{
				canvasGroup = canvas.GetComponent<CanvasGroup>();

				if (canvasGroup != null)
				{
					if (canvasGroup.alpha > 0)
					{
						canvasGroup.alpha = canvasGroup.alpha - (0.05f * speed * Time.deltaTime);
					}
				}
			}

		}

		if (Input.GetKeyDown(KeyCode.Escape) && backButton)
		{
			fadeEnabled = false;
			ChangeScene(lastScene);
		}
	}

}
