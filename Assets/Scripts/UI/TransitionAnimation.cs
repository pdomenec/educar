﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TransitionAnimation : MonoBehaviour
{

	public float speed = 300;

	private GameObject  panel;
	private bool        init = true;
	private GameObject  panelBack;

	void Start()
	{
		panel = new GameObject("Panel");
		panel.AddComponent<CanvasRenderer>();
		panel.AddComponent<Image>();

		RectTransform rectTransform = GetComponent<RectTransform>();

		panel.transform.localPosition = new Vector2(0, -(rectTransform.rect.height + speed));

		panel.GetComponent<RectTransform>().sizeDelta = new Vector2(
			rectTransform.rect.width,
			rectTransform.rect.height);

		panelBack = new GameObject("PanelBack");
		panelBack.AddComponent<CanvasRenderer>();
		panelBack.AddComponent<Image>();

		panelBack.GetComponent<RectTransform>().sizeDelta = new Vector2(
			rectTransform.rect.width,
			rectTransform.rect.height);

		Image image;
		GameObject controller = GameObject.Find("Controller Global");
		if (controller != null)
		{
			Texture2D screenshot = controller.GetComponent<Controller>().screenshot;

			image = panelBack.GetComponent<Image>();
			image.overrideSprite = Sprite.Create(screenshot,
				new Rect(0, 0,
					screenshot.width,
					screenshot.height),
					Vector2.zero, 100);

			image.color = new Color(
					image.color.r,
					image.color.g,
					image.color.b,
					image.color.a - 0.5f);
		}

		image = GetComponent<Image>();
		image.color = new Color(
				image.color.r,
				image.color.g,
				image.color.b,
				1f);

		panelBack.transform.SetParent(transform, false);
		panel.transform.SetParent(transform, false);

	}

	public IEnumerator setBackground()
	{

		yield return new WaitForEndOfFrame();

		init = false;

		RenderTexture renderTexture = new RenderTexture(Screen.width, Screen.height,
				16, RenderTextureFormat.ARGB32);
		RenderTexture.active = renderTexture;

		Camera.main.targetTexture = renderTexture;

		Image image = GetComponent<Image>();

		image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);

		image = panelBack.GetComponent<Image>();

		float alpha = image.color.a;

		image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);

		Camera.main.Render();

		Texture2D screenshot = new Texture2D(Screen.width, Screen.height,
				TextureFormat.RGB24, false);

		screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		screenshot.Apply();

		panel.GetComponent<Image>().overrideSprite = Sprite.Create(screenshot,
			new Rect(0, 0,
				screenshot.width,
				screenshot.height),
				Vector2.zero, 100);

		Camera.main.targetTexture = null;
		RenderTexture.active = null;

		Destroy(renderTexture);

		image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);

		image = GetComponent<Image>();

		image.color = new Color(image.color.r, image.color.g, image.color.b, 1f);
	}

	void Update()
	{


		if (init)
		{
			StartCoroutine(setBackground());
		}


		float pos = panel.transform.localPosition.y + speed;

		if (pos < 0)
		{
			panel.transform.localPosition = new Vector2(0, pos);
		}
		else
		{
			gameObject.SetActive(false);
		}
	}
}
