﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AlignBottom : MonoBehaviour
{

	public float refResolutionX = 1080f;
	public float refResolutionY = 2160f;

	public float offset  = 0;
	public float padding = 0;

	private float refAspectRatio;
	private float aspectRatio;


	// Start is called before the first frame update
	void Start()
	{
		refAspectRatio = refResolutionX/refResolutionY;
	}

	// Update is called once per frame
	void Update()
	{
		aspectRatio = (float)Screen.width/(float)Screen.height;

		float diff = aspectRatio - refAspectRatio;

		float pos = ( diff * refResolutionY );

		if (pos != 0)
		{
			pos += offset;
		}

		transform.localPosition = new Vector2(transform.localPosition.x, pos + padding);
	}
}
