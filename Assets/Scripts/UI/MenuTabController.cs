﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MenuTabController : MonoBehaviour
{

	private GameObject[] tabs;
	private GameObject[] views;

	public  GameObject   openView;

	private Color        normalColor;
	private Color        selectedColor;

	void Start()
	{
		normalColor   = GetComponent<Button>().colors.normalColor;
		selectedColor = GetComponent<Button>().colors.selectedColor;
	}

	public void OnClick()
	{
		tabs  = GameObject.FindGameObjectsWithTag("MenuTab");
		views = GameObject.FindGameObjectsWithTag("MenuView");

		ColorBlock cb = GetComponent<Button>().colors;

		foreach (GameObject tab in tabs)
		{
			cb.normalColor = normalColor;
			tab.GetComponent<Button>().colors = cb;
		}

		foreach (GameObject view in views)
		{
			view.SetActive(false);
		}


		cb.normalColor = selectedColor;
		GetComponent<Button>().colors = cb;

		if (openView) openView.SetActive(true);
	}

}
