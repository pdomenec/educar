﻿using UnityEngine;

public class PortraitOnly : MonoBehaviour
{
	// Start is called before the first frame update
	void Start()
	{
		Screen.orientation = ScreenOrientation.Portrait;
	}
}
