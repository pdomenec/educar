﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class PageSwiper : MonoBehaviour, IDragHandler, IEndDragHandler
{

	public GameObject scrollControl;

	public float percentThreshold = 0.2f;
	public float easing = 0.5f;

	private float []page;
	private int currentPage = 1;

	void Start()
	{
		page = new float[transform.childCount];
		float distance = 1f / (page.Length - 1f);

		for (int i=0; i < page.Length; ++i)
		{
			page[i] = distance * i;
		}
	}

	public void OnDrag(PointerEventData data)
	{
	}

	public void OnEndDrag(PointerEventData data)
	{

		float percentage = (data.pressPosition.x - data.position.x) / Screen.width;

		if(Mathf.Abs(percentage) >= percentThreshold)
		{
			if (percentage > 0 && currentPage < page.Length)
			{
				currentPage++;
				StartCoroutine(SmoothMove(scrollControl.GetComponent<Scrollbar>().value, page[currentPage-1], easing));
			}
			else if (percentage < 0 && currentPage > 1)
			{
				currentPage--;
				StartCoroutine(SmoothMove(scrollControl.GetComponent<Scrollbar>().value, page[currentPage-1], easing));
			}
		}
	}


	IEnumerator SmoothMove(float startpos, float endpos, float seconds){
		float t = 0f;
		while(t <= 1.0){
			t += Time.deltaTime / seconds;

			scrollControl.GetComponent<Scrollbar>().value = Mathf.Lerp(startpos, endpos, Mathf.SmoothStep(0f, 1f, t));
			yield return null;
		}
	}
}
