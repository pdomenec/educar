﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerLocal : MonoBehaviour
{

	GameObject controller;

	public bool backButton = false;

	void Start()
	{
		controller = GameObject.Find("Controller Global");
	}

	void Update()
	{
		if (controller != null)
		{
			controller.GetComponent<Controller>().backButton = backButton;
		}
	}

	public void OnClick(string scene)
	{
		if (controller != null)
		{
			controller.GetComponent<Controller>().OnClick(scene);
		}
	}

	public void OnClickWithFade(string scene)
	{
		if (controller != null)
		{
			controller.GetComponent<Controller>().OnClickWithFade(scene);
		}
	}


}
